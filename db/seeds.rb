# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Hobby.delete_all

Hobby.create(
  name: 'Play basketball'
)
Hobby.create(
  name: 'Play computer games'
)
Hobby.create(
  name: 'See Movie'
)
Hobby.create(
  name: 'Draw'
)
