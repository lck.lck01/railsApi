Rails.application.routes.draw do

  namespace :api, path: '/' do
    resources :hobbies
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
